<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->truncate();
        DB::table('settings')->insert([
            [
                'key' => 'site_title',
                'value' => 'Celebrity Wish'
            ],
            [
                'key' => 'site_logo',
                'value' => '/asset/logo.png',
            ],
            [
                'key' => 'site_mail_icon',
                'value' => '/asset/logo.png',
            ],
            [
                'key' => 'site_fav_icon',
                'value' => '/asset/icon.png',
            ],
            [
                'key' => 'site_copyright',
                'value' => '&copy; '.date('Y').' Celebrity Wish',
            ],
            [
                'key' => 'contact_number',
                'value' => '9876543210'
            ],
            [
                'key' => 'contact_email',
                'value' => 'admin@kifiya.com'
            ],
            [
                'key' => 'page_privacy',
                'value' => ''
            ],
            [
                'key' => 'contact_text',
                'value' => 'We will Contact you soon'
            ],
            [
                'key' => 'contact_title',
                'value' => 'Celebrity Wish Help'
            ]
        ]);
    }
}
