<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class DemoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->truncate();
        DB::table('users')->insert([[
            'first_name' => 'Demo',
            'last_name' => 'Demo',
            'email' => 'demo@demo.com',
            'password' => bcrypt('123456'),
            'mobile' => '9876543210',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]]);

        DB::table('celebrities')->truncate();
        DB::table('celebrities')->insert([[
            'first_name' => 'Celeb',
            'last_name' => 'Demo',
            'email' => 'celeb@demo.com',
            'password' => bcrypt('123456'),
            'mobile' => '9876543210',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]]);
    }
}
