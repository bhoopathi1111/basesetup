<?php

/*
|--------------------------------------------------------------------------
| Authentication Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['prefix' => 'admin'], function () {
    Route::get('/login', 'AdminAuth\LoginController@showLoginForm');
    Route::post('/login', 'AdminAuth\LoginController@login');
    Route::post('/logout', 'AdminAuth\LoginController@logout');

    Route::post('/password/email', 'AdminAuth\ForgotPasswordController@sendResetLinkEmail');
    Route::post('/password/reset', 'AdminAuth\ResetPasswordController@reset');
    Route::get('/password/reset', 'AdminAuth\ForgotPasswordController@showLinkRequestForm');
    Route::get('/password/reset/{token}', 'AdminAuth\ResetPasswordController@showResetForm');
});

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'UserController@index');

Auth::routes();

Route::get('privacy', function () {
    $page = 'page_privacy';
    $title = 'Privacy Policy';
    return view('static', compact('page', 'title'));
});


/*
|--------------------------------------------------------------------------
| User Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/dashboard', 'UserController@dashboard');

// user profiles
Route::get('/profile',      'HomeController@profile');
Route::get('/edit/profile', 'HomeController@edit_profile');
Route::post('/profile',     'HomeController@update_profile');

// update password
Route::get('/change/password',  'HomeController@change_password');
Route::post('/change/password', 'HomeController@update_password');

Route::group(['prefix' => 'celebrity'], function () {
  Route::get('/login', 'CelebrityAuth\LoginController@showLoginForm')->name('login');
  Route::post('/login', 'CelebrityAuth\LoginController@login');
  Route::post('/logout', 'CelebrityAuth\LoginController@logout')->name('logout');

  Route::get('/register', 'CelebrityAuth\RegisterController@showRegistrationForm')->name('register');
  Route::post('/register', 'CelebrityAuth\RegisterController@register');

  Route::post('/password/email', 'CelebrityAuth\ForgotPasswordController@sendResetLinkEmail')->name('password.request');
  Route::post('/password/reset', 'CelebrityAuth\ResetPasswordController@reset')->name('password.email');
  Route::get('/password/reset', 'CelebrityAuth\ForgotPasswordController@showLinkRequestForm')->name('password.reset');
  Route::get('/password/reset/{token}', 'CelebrityAuth\ResetPasswordController@showResetForm');
});
