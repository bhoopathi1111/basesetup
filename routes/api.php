<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/initsetup', function () {
    return Setting::all();
});
Route::post('/signup' , 'UserApiController@signup');
Route::post('/logout' , 'UserApiController@logout');
Route::post('/verify' , 'UserApiController@verify');
Route::post('/forgot/password',     'UserApiController@forgot_password');
Route::post('/reset/password',      'UserApiController@reset_password');

Route::group(['middleware' => ['auth:api']], function () {

	// user profile

	Route::post('/change/password' , 	'UserApiController@change_password');
	Route::post('/update/location' , 	'UserApiController@update_location');
	Route::get('/details' , 			'UserApiController@details');
	Route::get('/provider' , 			'UserApiController@provider');
	Route::post('/update/profile' , 	'UserApiController@update_profile');

	// services

	Route::get('/services' , 'UserApiController@services');

	
	// payment

	Route::post('/payment' , 	'PaymentController@payment');
	Route::post('/add/money' , 	'PaymentController@add_money');

	

	// promocode

	Route::get('/promocodes' , 		'UserApiController@promocodes');
	Route::post('/promocode/add' , 	'UserApiController@add_promocode');

	// card payment

    Route::resource('card', 'Resource\CardResource');    

	Route::get('/help' , 'UserApiController@help_details');
});
