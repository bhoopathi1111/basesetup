@extends('admin.layout.base')

@section('title', 'Add Category ')

@section('content')

<div class="content-area py-1">
    <div class="container-fluid">
    	<div class="box box-block bg-white">
            <a href="{{ route('admin.category.index') }}" class="btn btn-default pull-right"><i class="fa fa-angle-left"></i> Back</a>

			<h5 style="margin-bottom: 2em;">Add Category</h5>

            <form class="form-horizontal" action="{{route('admin.category.store')}}" method="POST" enctype="multipart/form-data" role="form">
            	{{csrf_field()}}
				<div class="form-group row">
					<label for="name" class="col-xs-12 col-form-label">Category Name</label>
					<div class="col-xs-10">
						<input class="form-control" type="text" value="{{ old('category_name') }}" name="category_name" required id="name" placeholder="Category Name">
					</div>
				</div>

				<div class="form-group row">
					<label for="name" class="col-xs-12 col-form-label">Description</label>
					<div class="col-xs-10">
						<textarea class="form-control" type="text" name="description" required id="description" placeholder="Category Description" rows='5'> </textarea>
					</div>
				</div>

				<div class="form-group row">
					<label for="picture" class="col-xs-12 col-form-label">Category Image</label>
					<div class="col-xs-10">
						<input type="file" accept="image/*" name="image" class="dropify form-control-file" id="picture" aria-describedby="fileHelp">
					</div>
				</div>
				
				<div class="form-group row">
					<label for="status" class="col-xs-12 col-form-label">Status</label>
					<div class="col-xs-10">
						<select class="form-control" name="status"  id="status" required>
							<option value="0">Active</option>
							<option value="1" selected>Inactive</option>
						</select>
					</div>
				</div>

				<div class="form-group row">
					<label for="zipcode" class="col-xs-2 col-form-label"></label>
					<div class="col-xs-10">
						<button type="submit" class="btn btn-primary">Add Category</button>
						<a href="{{route('admin.category.index')}}" class="btn btn-default">Cancel</a>
					</div>
				</div>
			</form>
		</div>
    </div>
</div>

@endsection
