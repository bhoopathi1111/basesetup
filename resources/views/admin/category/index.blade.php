@extends('admin.layout.base')

@section('title', 'Categories')

@section('content')

    <div class="content-area py-1">
        <div class="container-fluid">            
            <div class="box box-block bg-white">         
                <h5 class="mb-1">Categories</h5>
                <a href="{{ route('admin.category.create') }}" style="margin-left: 1em;" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Add New Category</a>
                <table class="table table-striped table-bordered dataTable" id="table-2">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Category Name</th>
                            <th>Image</th>
                            <th>Description</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($categories as $index => $value)
                        <tr>
                            <td>{{$index + 1}}</td>
                            <td>{{$value->category_name}}</td>
                            <td>
                                @if($value->image) 
                                    <img src="{{img($value->image)}}" style="height: 50px" >
                                @else
                                    N/A
                                @endif
                            </td>
                            <td>{{$value->description}}</td>
                            <td>
                                 @if($value->status==0) 
                                    <span style="color: green;" > Active </span>
                                @else
                                    <span style="color: red;" > Inactive </span>
                                @endif
                            </td>
                            <td>
                                <form action="{{ route('admin.category.destroy', $value->id) }}" method="POST">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="_method" value="DELETE">
                                    <a href="{{ route('admin.category.edit', $value->id) }}" class="btn btn-info"><i class="fa fa-pencil"></i> Edit</a>
                                    <button class="btn btn-danger" onclick="return confirm('Are you sure?')"><i class="fa fa-trash"></i> Delete</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>ID</th>
                            <th>Category Name</th>
                            <th>Image</th>
                            <th>Description</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
            
        </div>
    </div>
@endsection