@extends('admin.layout.base')

@section('title', 'Update Category ')

@section('content')

<div class="content-area py-1">
    <div class="container-fluid">
    	<div class="box box-block bg-white">
    	    <a href="{{ route('admin.category.index') }}" class="btn btn-default pull-right"><i class="fa fa-angle-left"></i> Back</a>

			<h5 style="margin-bottom: 2em;">Update Category</h5>

            <form class="form-horizontal" action="{{route('admin.category.update', $category->id )}}" method="POST" enctype="multipart/form-data" role="form">
            	{{csrf_field()}}
            	<input type="hidden" name="_method" value="PATCH">
				<div class="form-group row">
					<label for="name" class="col-xs-2 col-form-label">Category Name</label>
					<div class="col-xs-10">
						<input class="form-control" type="text" value="{{ $category->category_name }}" name="category_name" required id="name" placeholder="Category Name">
					</div>
				</div>

				<div class="form-group row">
					<label for="name" class="col-xs-12 col-form-label">Description</label>
					<div class="col-xs-10">
						<textarea class="form-control" type="text" name="description" required id="description" placeholder="Category Description" rows='5'>{{ $category->description}}</textarea>
					</div>
				</div>

				<div class="form-group row">					
					<label for="image" class="col-xs-2 col-form-label">Category Image</label>
					<div class="col-xs-10">
						@if(isset($category->image))
	                    	<img style="height: 90px; margin-bottom: 15px; border-radius:2em;" src="{{img($category->image)}}">
	                    @endif
						<input type="file" accept="image/*" name="image" class="dropify form-control-file" id="image" aria-describedby="fileHelp">
					</div>
				</div>

				<div class="form-group row">
					<label for="status" class="col-xs-12 col-form-label">Status</label>
					<div class="col-xs-10">
						<select class="form-control" name="status"  id="status" required>
							<option value="0" @if($category->status == 0) selected @endif >Active</option>
							<option value="1" @if($category->status == 1) selected @endif >Inactive</option>
						</select>
					</div>
				</div>


				<div class="form-group row">
					<label for="zipcode" class="col-xs-2 col-form-label"></label>
					<div class="col-xs-10">
						<button type="submit" class="btn btn-primary">Update Category</button>
						<a href="{{route('admin.category.index')}}" class="btn btn-default">Cancel</a>
					</div>
				</div>
			</form>
		</div>
    </div>
</div>

@endsection