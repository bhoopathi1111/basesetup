@extends('user.layout.app')

@section('content')

<section class="">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <img src="asset/bg.png" class="" style="width: 100%;">
      </div>
    </div>
  </div>
</section>

<section class="">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="hero-content">
          <h2>Featured</h2>
          <div class="">
            @foreach($featured as $value)
              <div class="col-md-2">
                  <img src="{{$value->picture}}" width="100%">
                  <h3>{{$value->first_name}} {{$value->middle_name}} {{$value->last_name}}</h3>
                  <span>{{$value->role}}</span>
              </div>
            @endforeach
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection


@section('scripts')
<script type="text/javascript">
  $('#global_search_btn').click(function {
    var search = $('#global_search').val();
    var url = "/dashboard/?search="+search;
    window.location.href(url);
  });  
</script>
@endsection    