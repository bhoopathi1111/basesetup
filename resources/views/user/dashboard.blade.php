@extends('user.layout.base')

@section('title', 'Dashboard ')

@section('content')
<div class="row">
  <div class="col-md-12">
      <div class="dash-content">
          <div class="row no-margin">
              <div class="col-md-12">
                  <h4 class="page-title">Categories</h4>
              </div>
          </div>
          
          @include('common.notify')

          <div class="services row no-margin">
            @foreach($categories as $index => $category)
              <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12" onclick="categoryToggle({{$index+1}})">
                  <div class="services-sel-box">
                      <a href="javascript:void(0)" class="sel-ser-link">
                          <div class="sel-ser-img bg-img" style="background-image: url({{img($category->image)}});"></div>
                          <h3 class="sel-ser-tit">{{$category->category_name}}</h3>
                      </a>
                  </div>
              </div>
            @endforeach

            <div  class="row" >
              <div class="col-md-12">
                  <div class="dash-content services">
                      <div class="row no-margin">
                          <div class="col-md-12">
                              <h4 class="page-title">Services</h4>
                          </div>
                      </div>
                      @foreach($categories as $index => $category)
                        <div class="servicesDiv" id="service_{{$index+1}}" style="display: none;">                           
                          <div class="row">                            
                            @foreach($category->serviceType as $index => $service)
                              <div class="col-xs-12 col-md-4 col-lg-3" style="cursor: pointer;" onclick="serviceToggle({{$service->id}})">
                                <div class="popular-services-box">
                                  <div class="img-block">
                                    <img src="{{img($service->image)}}" class="img-responsive">
                                  </div>
                                  <div class="content-block">
                                    <h3>{{$service->name}}</h3>
                                    @if (count($service->serviceSubCategory) <= 0)
                                      <a href="{{url('dashboard')}}?service={{$service->id}}" class="viewMore-btn">Book Now</a>
                                    @endif
                                  </div>
                                </div>
                              </div>
                            @endforeach
                          </div>  

                          <div class="row"> 
                            @foreach($category->serviceType as $index => $service)
                              @if (count($service->serviceSubCategory) > 0)
                                <div class="col-md-12 subServicesDiv" id="subServices_{{$service->id}}"  style="display: none;">
                                    <div class="dash-content subServices">
                                        <div class="row no-margin">
                                            <div class="col-md-12">
                                                <h4 class="page-title">Sub Service</h4>
                                            </div>
                                        </div>
                                        @foreach($service->serviceSubCategory as $index => $serviceSub)
                                        <div class="row justify-content-center">
                                            <div class="col-sm-12 col-md-6 col-lg-4">
                                              <div class="popular-services-box inner">
                                                <div class="img-block">
                                                  <img src="{{img($serviceSub->image)}}" class="img-responsive">
                                                </div>
                                                <div class="content-block">
                                                  <h3>{{$serviceSub->name}}</h3>                                                
                                                  <a href="{{url('dashboard')}}?subService={{$service->id}}" class="viewMore-btn">Book Now</a>
                                                </div>
                                              </div>
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                              @endif
                            @endforeach
                          </div>
                        </div>
                      @endforeach
                  </div>
              </div>               
            </div>
          </div>
      </div>
  </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
  $(document).ready(function(){
      categoryToggle(1);
  });

  function categoryToggle(id) {
    $('.servicesDiv').hide();
    $('#service_' + id).slideToggle('fast');
  }

  function serviceToggle(id) {
    $('.subServicesDiv').hide();
    $('#subServices_' + id).slideToggle('fast');
  }
</script>
  

@endsection