<header style="margin-bottom: 61px;">
    <nav class="navbar navbar-fixed-top">
      <div class="container-fluid">
        <div class="col-md-12">
            <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>             

              <a class="navbar-brand logo-txt" href="{{url('/')}}">
                <img src="{{Setting::get('site_logo')}}">
              </a>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">                          
                <ul class="nav navbar-nav navbar-right">                   
                  <li class="">
                    <a href="{{url('/')}}"><i class="fa fa-search" aria-hidden="true"></i> Search</a>
                  </li>
                  <li class="">
                    <a href="{{url('/')}}"><i class="fa fa-th-large" aria-hidden="true"></i> Categories</a>
                  </li>
                  @if(!Auth::check())
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('login')}}"><i class="fa fa-user" aria-hidden="true"></i> Login</a>
                    </li>
                  @else 
                    <li class="menu-drop">
                      <div class="dropdown">
                          <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-user" aria-hidden="true"></i> {{Auth::user()->first_name}} {{Auth::user()->last_name}}
                          <span class="caret"></span></button>
                          <ul class="dropdown-menu">                            
                            <li><a href="{{url('profile')}}">@lang('user.profile.profile')</a></li>
                            <li><a href="{{url('change/password')}}">@lang('user.profile.change_password')</a></li>
                            <li><a href="{{ url('/logout') }}"
                                    onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">@lang('user.profile.logout')</a></li>
                                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                          </ul>
                        </div>
                    </li>
                  @endif
                </ul>
                <div class="seaSrch-form" style="display: none;">
                  <input type="text" name="search-form" placeholder="What product or service are you looking for?" id="global_search" />
                  <button type="button" id="global_search_btn"><i class="fa fa-search" aria-hidden="true"></i></button>
                </div>
            </div>
        </div>
      </div>
    </nav>
</header>