<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container">
        <a class="navbar-brand" href="{{url('dashboard')}}"><!-- <img src="{{Setting::get('site_logo')}}" style="width: 20px;"> --> Kifiya</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active">
                  <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                </li>
                @if(Auth::check())
                    <li class="nav-item active">
                      <a class="nav-link" href="{{url('/dashboard')}}">Dashboard</a>
                    </li>
                    <li class="nav-item dropdown">
                          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            {{Auth::user()->first_name}} {{Auth::user()->last_name}}
                          </a>
                          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <ul class="">
                                <li><a href="{{url('trips')}}">@lang('user.my_trips')</a></li>
                                <li><a href="{{url('profile')}}">@lang('user.profile.profile')</a></li>
                                <li><a href="{{url('change/password')}}">@lang('user.profile.change_password')</a></li>
                                <li><a href="{{ url('/logout') }}"
                                        onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();">@lang('user.profile.logout')</a></li>
                                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                              </ul>                       
                          </div>
                    </li>
                @else
                    <li class="nav-item">
                      <a class="nav-link" href="{{route('login')}}">Login</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="{{route('register')}}">Signup</a>
                    </li> 
                    <li class="nav-item">
                      <a class="nav-link" href="{{url('/provider/login')}}">Become Service Men</a>
                    </li>
                    <!-- <li class="nav-item dropdown">
                      <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        All Services
                      </a>
                      <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="#">Action</a>
                        <a class="dropdown-item" href="#">Another action</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">Something else here</a>
                      </div>
                    </li> -->
                @endif   
            </ul>
        </div>
    </div>
</nav>