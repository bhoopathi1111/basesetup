@extends('user.layout.base')

@section('title', 'Wallet ')

@section('content')

<div class="col-md-9">
    <div class="dash-content">
        <div class="row no-margin">
            <div class="col-md-12">
                <h4 class="page-title">@lang('user.my_wallet')</h4>
            </div>
        </div>
        @include('common.notify')

        <div class="row no-margin">
            <form action="{{url('add/money')}}" method="POST">
            {{ csrf_field() }}
                <div class="col-md-6">
                     
                    <div class="wallet">
                        <h4 class="amount">
                        	<span class="price">{{currency(Auth::user()->wallet_balance)}}</span>
                        	<span class="txt">@lang('user.in_your_wallet')</span>
                        </h4>
                        <a href="#"  data-toggle="modal" data-target="#add-money-modal" class="addMoneyBtn">Add money</a>
                    </div>                                                               

                </div>
                @if(Setting::get('CARD') == 1)
                <div class="col-md-6">
                     @if($cards->count() > 0)
                    <h6><strong>@lang('user.add_money')</strong></h6>

                    <div class="input-group full-input">
                        <input type="number" class="form-control" name="amount" placeholder="Enter Amount" >
                    </div>
                    <br>
                    
                        <select class="form-control" name="card_id">
	                      @foreach($cards as $card)
	                        <option @if($card->is_default == 1) selected @endif value="{{$card->card_id}}">{{$card->brand}} **** **** **** {{$card->last_four}}</option>
	                      @endforeach
                        </select>
                    
                    
                    <button type="submit" class="full-primary-btn fare-btn">@lang('user.add_money')</button> 

                    @else
                        <p class="no-card">Please Add card to continue.<br>Click here to <a href="{{url('payment')}}">add card</a></p>
                    @endif

                </div>
                @endif
            </form>
        </div>

    </div>
</div>


<!-- Add Card Modal -->
    <div id="add-money-modal" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content" >
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Add Money</h4>
          </div>
            <form id="money-form" action="" method="POST" >
                {{ csrf_field() }}
          <div class="modal-body">
            <div class="row no-margin">
                <div class="form-group col-md-12 col-sm-12">
                    <label>Enter amount</label>
                    <input required type="text" class="form-control" placeholder="Enter amount">
                </div>
            </div>
          </div>

          <div class="modal-footer">
            <button type="submit" class="btn btn-default">Add Money</button>
          </div>
        </form>

        </div>

      </div>
    </div>

@endsection