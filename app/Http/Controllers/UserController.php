<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\Helper;
use Auth;
use Exception;
use Storage;
use Setting;
use App\Category;
use App\Celebrity;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $categories = Category::get();

        $featured = Celebrity::where('status', 1)->take(15)->get();
        //dd($featured);
        return view('index', compact('categories', 'featured'));
       
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function dashboard(Request $request)
    {

        $userApiController = new UserApiController();
        if (Auth::check()) {

            $Response = $userApiController->request_status_check()->getData();

            if(!empty($Response->data))
            {
                return view('user.ride.waiting')->with('request',$Response->data[0]);
            }
        }

        if($request->has('service')){
            if (Auth::check()) {
                $cards = (new Resource\CardResource)->index();
                $service = (new Resource\ServiceResource)->show($request->service);
                return view('user.request',compact('cards','service'));
            } else {
                return redirect('/login');
            }
        } elseif($request->has('subService')){
            if (Auth::check()) {
                $cards = (new Resource\CardResource)->index();
                $service = (new ServiceSubCategoryController)->show($request->subService);
                return view('user.request',compact('cards','service'));
            } else {
                return redirect('/login');
            }
        } else{
            $categories = Category::with(['serviceType' => function($q) {
                        /*if($request->has('search')){
                            $q->where('serviceSubCategory');
                        }*/
                        $q->with('serviceSubCategory');
                    }])->get();
            //dd($categories);
            $services = $userApiController->services();
            return view('user.dashboard',compact('services','categories'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $this->validate($request, [
            'category_name' => 'required|max:255',           
            'image' => 'mimes:png,jpg,jpeg'
        ]);

        try{
            $categoryData = $request->all();
            
            if ($request->hasFile('image')) {
                $categoryData['image'] = $request->image->store('category');
            }
            //dd($categoryData);
            $category = Category::create($categoryData);

            return back()->with('flash_success','Category Saved Successfully');

        } 

        catch (Exception $e) {
            return back()->with('flash_errors', 'Category Not Found');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            return Category::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return $e;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $category = Category::findOrFail($id);
            return view('admin.category.edit',compact('category'));
        } catch (ModelNotFoundException $e) {
            return $e;
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\categoryType  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $this->validate($request, [
            'category_name' => 'required|max:255',
            'image' => 'mimes:png,jpeg,jpg'
        ]);

        try {

            $category = Category::findOrFail($id);

            if ($request->hasFile('image')) {
                $category->image  = $request->image->store('category');
            }

            $category->category_name = $request->category_name;
            $category->status = $request->status;
            $category->save();

            return redirect()->route('admin.category.index')->with('flash_success', 'Category Updated Successfully');    
        } 

        catch (ModelNotFoundException $e) {
            return back()->with('flash_errors', 'Category Not Found');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            Category::find($id)->delete();
            return back()->with('message', 'Category deleted successfully');
        } 
        catch (Exception $e) {
            return back()->with('flash_errors', 'Category Not Found');
        }
    }
}
